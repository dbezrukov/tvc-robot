#!/bin/bash
# Robot running daemon in BASH

if [ $# -eq 0 ]
  then
    echo "usage: runrobot name ip port"
    echo "sample: 'runrobot alba 10.0.0.170 8088' allows an external vnc viewer to connect to 10.0.0.170:8088"
    exit 1
fi

export DISPLAY=:$2
Xvfb $DISPLAY -screen 0 1024x768x16 > /dev/null 2>&1 &
x11vnc -display $DISPLAY -nopw -listen $3 -xkb -bg -rfbport $4 -forever

echo "Starting Chrome.."

PROGRAM="google-chrome --disk-cache-dir=/dev/null --disk-cache-size=1 --media-cache-size=1  --no-default-browser-check --no-first-run --user-data-dir=tvc-robot-profiles/$1 videocafe.com"

exec > /dev/null 
exec 2>/dev/null
exec < /dev/null

(
    trap "" TERM 
    while true; do
       DATENOW=$(date +"%m-%d-%Y")
       TIMENOW=$(date +"%r")
       echo $DATENOW $TIMENOW - Starting Chrome for $1 >> restarts.log
	   $PROGRAM &
       PID=$!
       sleep 43200
       kill -15 $PID
    done
)&
