#!/bin/bash

while [ true ]
do
    #tvc-robot/playvideo.sh $3 $4 &
    #tvc-robot/playsound.sh $1 $2

    gst-launch-0.10 -v filesrc location=$3 \
       ! videoparse width=352 height=288 ! v4l2sink device=/dev/video$4 &
    aplay -D hw:$2,0,0 $1 &
    wait
done
